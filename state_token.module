<?php

/**
 * @file
 * Hooks for the state_token module.
 */

/**
 * Implements hook_token_info().
 */
function state_token_token_info(): array {
  $info = [
    'types' => [
      'state' => [
        'name' => t('State API'),
        'description' => t('Tokens returning values from states'),
      ],
    ],
  ];

  foreach (\Drupal::keyValue('state')->getAll() as $name => $value) {
    $info['tokens']['state'][$name] = [
      'name' => $name,
      'description' => t('Value of the @name state', ['@name' => $name]),
    ];
  }

  return $info;
}

/**
 * Implements hook_tokens().
 */
function state_token_tokens(string $type, array $tokens): array {
  $state = \Drupal::state();
  $replacements = [];

  if ($type == 'state') {
    foreach ($tokens as $name => $original) {
      $replacements[$original] = $state->get($name);
    }
  }

  return $replacements;
}
